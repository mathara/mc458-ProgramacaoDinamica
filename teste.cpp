/*
Matheus Mendes Araujo 156737


*/
#include <iostream>
#include <vector>
#include <stdio.h>
using namespace std;

typedef long int lint;

//imprime vetor
void imprime(vector<lint> v){
  for (lint i = 0; i < v.size(); i++) {
    printf("%ld ", v[i] );
  }
  printf("\n");
}

void recebe_valors(vector<lint> &v) {
  for (lint i = 0; i < v.size()-1; i++) {
    cin >> v[i];
  }
}

lint avalia_chance_pedidos(lint dia, vector<lint> &c, vector<lint> &d, lint H) {
  lint cpedidos;
    cpedidos = c[dia]/(H * d[dia]);

    return cpedidos;
}

void calcula_custo_total(vector<lint> &c, vector<lint> &d, vector<lint> &p, lint H){
  lint acumulador = 0;
  lint flagt;
  lint melhor;
  p[p.size()-1] = 0;

  for (lint i = p.size()-2; i >= 0; i--) {
    melhor = c[i] + p[i+1];
    acumulador = c[i];
    flagt = 1;
    printf("--------III---------%ld\n",i );

    for (lint j = i+1; (j < p.size()-1) && (flagt == 1); j++) {
      printf("d[j] %ld j %ld i %ld H %ld\n",d[j],j, i, H );
      printf("acumuludar%ld + (d[j]*(j-i)*H %ld\n",acumulador, (d[j]*(j-i)*H) );
      acumulador +=(d[j]*(j-i)*H);
      printf("acumuludar%ld \n",acumulador );

      if (acumulador > melhor){
        flagt = 0;
      }
      else if(melhor > acumulador +p[j+1]){
        melhor = acumulador +p[j+1];
      }

    }

    p[i] = melhor;
  }

  printf("%ld\n", p[0]);
}

int main() {
  //variáveis
  lint cnoite;
  lint dias;

  //recebe o custo de pernoite
  cin >> cnoite;
  //recebe a quantidade de dias
  cin >> dias;

  //Cria vetores custo e demanda
  vector<lint> custos(dias+1);
  vector<lint> demanda(dias+1);
  vector<lint> pedidos(dias+1);

  //recebe valores do vector
  recebe_valors(custos);
  recebe_valors(demanda);

  calcula_custo_total(custos, demanda, pedidos, cnoite);
  imprime(pedidos);
}
