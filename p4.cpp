/*
Matheus Mendes Araujo 156737

A resolução se baseia em programação dinâmica. Um vetor de pedidos foi criado,
ele é percorrido de trás para frente e armazeno o menor cursto de encomenda até
aquele dia. Para descobrir isso, ele testa todas possiblidades utilizando uma
var acumulador, quando acumuludor passa a melhor solução descoberta, não é
preciso testar as próximas possibilidades, pois será maiores.

*/
#include <iostream>
#include <vector>
#include <stdio.h>
using namespace std;

typedef long int lint;

//imprime vetor
void imprime(vector<lint> v){
  for (lint i = 0; i < v.size(); i++) {
    printf("%ld ", v[i] );
  }
  printf("\n");
}

//recebe valores do vetor
void recebe_valors(vector<lint> &v) {
  for (lint i = 0; i < v.size()-1; i++) {
    cin >> v[i];
  }
}

void calcula_custo_total(vector<lint> &c, vector<lint> &d, vector<lint> &p, lint H){
  lint acumulador = 0;
  lint flagt;
  lint melhor;
  //um dia de folga com 0, pois não interfere no resultado
  p[p.size()-1] = 0;

  for (lint i = p.size()-2; i >= 0; i--) {
    /*melhor solução inicial : encomenda no dia + a melhor solução
    encontrada para o próximo dia */
    melhor = c[i] + p[i+1];
    //inicialmente tem apenas o custo do dia
    acumulador = c[i];
    //indica se ainda deve continuar testando as possiblidades
    flagt = 1;

    //testa as possibilidades buscando a menor
    for (lint j = i+1; (j < p.size()-1) && (flagt == 1); j++) {
      //acumula o custo de manter o sorvete até outro dia
      acumulador += (d[j]*(j-i)*H);

      //acumuludor passa o melhor custo, não preciso testar próximas combinações
      if (acumulador > melhor){
        flagt = 0;
      }
      /*verifica o custo acumulador + a melhor solucao do próximo dia, com a
      melhor solução atual*/
      else if(melhor > acumulador +p[j+1]){
        melhor = acumulador +p[j+1];
      }
    }
    //armazena o menor custo encontrado para o dia
    p[i] = melhor;
  }

  printf("%ld\n", p[0]);
}

int main() {
  //variáveis
  lint cnoite;
  lint dias;

  //recebe o custo de pernoite
  cin >> cnoite;
  //recebe a quantidade de dias
  cin >> dias;

  //Cria vetores custo e demanda
  vector<lint> custos(dias+1);
  vector<lint> demanda(dias+1);
  vector<lint> pedidos(dias+1);

  //recebe valores do vector
  recebe_valors(custos);
  recebe_valors(demanda);

  //calcula menor custo
  calcula_custo_total(custos, demanda, pedidos, cnoite);
}
