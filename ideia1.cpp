/*
Matheus Mendes Araujo 156737


*/
#include <iostream>
#include <vector>
#include <stdio.h>
using namespace std;

typedef long int lint;

//imprime vetor
void imprime(vector<lint> v){
  for (lint i = 0; i < v.size(); i++) {
    printf("%ld ", v[i] );
  }
  printf("\n");
}

void recebe_valors(vector<lint> &v) {
  for (lint i = 0; i < v.size(); i++) {
    cin >> v[i];
  }
}

void avalia_chance_pedidos(vector<lint> &c, vector<lint> &d, lint H) {
  lint cpedidos;
  for (lint i = 0; i < c.size(); i++) {
    cpedidos = c[i]/(H * d[i]);
    printf("n :%ld %ld %ld %ld\n", cpedidos, c[i], H, d[i]);
  }
}

lint avalia_chance_pedidos(lint dia, vector<lint> &c, vector<lint> &d, lint H) {
  lint cpedidos;
    cpedidos = c[dia]/(H * d[dia]);

    return cpedidos;
}

void monta_pedidos(vector<lint> &c, vector<lint> &d, vector<lint> &p, lint H){
  lint vd, melhor = 0, custoA,custoB, menorc = 0;
  lint custos = c[0];
  p[0] = d[0];

  for (lint i = 1; i < c.size(); i++) {
    vd =  avalia_chance_pedidos(i,c,d,H);

    if (vd == 0 ){
      if (p[i] == 0) {
        custos = custos + c[i];
      }
      p[i] = p[i] + d[i];
    }
    else{
      melhor = i;
      menorc = c[i];
      for (lint j = 1; (j <= vd) &&  (i -j >= 0); j++) {
        custoA = 0;
        custoB = 0;

        if (p[i-j] == 0)
          custoA = custoA + c[i-j];
        custoA = custoA + d[i]*j*H;

        if (p[melhor] == 0)
          custoB = custoB + c[melhor];
        custoB = custoB + d[i]*(i-melhor)*H;

        if (custoA <= custoB){
          melhor = i-j;
          menorc = custoA;
        }
      }
      custos = custos + menorc;
      p[melhor] = p[melhor] + d[i];
    }
  }
  printf("%ld\n", custos );
}

void calcula_custo_total(vector<lint> &c, vector<lint> &d, vector<lint> &p, lint H){
  lint custo = 0;
  for (lint i = 0; i < p.size(); i++) {
    if (p[i] != 0){
      custo = custo + c[i];
    }
  }

  for (lint i = 0; i < p.size(); i++) {
    p[i] = p[i] - d[i];
    if (p[i] != 0){
      custo = custo + p[i]*H;
      p[i+1] = p[i+1] + p[i];
      p[i] = 0;
    }
  }

  printf("%ld\n", custo );
}

int main() {
  //variáveis
  lint cnoite;
  lint dias;

  //recebe o custo de pernoite
  cin >> cnoite;
  //recebe a quantidade de dias
  cin >> dias;

  //Cria vetores custo e demanda
  vector<lint> custos(dias);
  vector<lint> demanda(dias);
  vector<lint> pedidos(dias);

  //recebe valores do vector
  recebe_valors(custos);
  recebe_valors(demanda);

  monta_pedidos(custos, demanda, pedidos, cnoite);
  //calcula_custo_total(custos, demanda, pedidos, cnoite);
}
